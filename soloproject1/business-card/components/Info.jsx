import React from "react";
import pfp from "../components/icons/pfp.jpg"

export default function Info () {
    return (
        <div className="info">
                    <img className="pfp" src={pfp} alt="alt" />
            <h1 className="name">Tobias Le</h1>
            <h4 className="role">Frontend Developer</h4>
            <p className="website">My website!</p>
            <div className="buttons">
                <div className="email button">Email</div>
                <div className="linkedin button">LinkedIn</div>
            </div>
        </div>
    )
}
import React from "react";

export default function About () {
    return (
        <div className="about">
        <h1 className="aboutTitle">About</h1>

        <p className="paragraph">I am frontend developer and also a student
            at the Czech Technical University. I am studying
            Software Engineering and Technology.
        </p>
        </div>
    )
}
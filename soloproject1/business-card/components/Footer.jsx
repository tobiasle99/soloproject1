import React from "react";
import icon1 from "../components/icons/Facebook.png"
import icon2 from "../components/icons/Twitter.png"
import icon3 from "../components/icons/GitHub.png"
import icon4 from "../components/icons/Instagram.png"

export default function Footer () {
    return (
        <div className="footer">
            <img className="icon" src={icon1} alt="alt" />
            <img className="icon" src={icon2} alt="alt" />
            <img className="icon" src={icon3} alt="alt" />
            <img className="icon" src={icon4} alt="alt" />

        </div>

    )
}
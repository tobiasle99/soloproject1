import React from "react";

export default function Interests () {
    return (
        <div className="interests">
        <h1 className="aboutTitle">Interests</h1>
        <p className="paragraph">Video games, e-sports, social media, reading
            programming, puzzles, escape rooms
        </p>
        </div>
    )
}
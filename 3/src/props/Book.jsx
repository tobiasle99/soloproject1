import React from "react";

export default function Book(props) {
    return(
        <div className="book">
            <h1>Name: {props.name}</h1>
            <h2>Author: {props.author} </h2>
        </div>
    )
}
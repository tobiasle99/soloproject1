import React, { useState, useEffect } from "react";
import "./App.css"; // Import the CSS file for the dark-themed colorful style

const App = () => {
  const [books, setBooks] = useState([]);

  useEffect(() => {
    // Fetch book data from the API endpoint
    fetch("http://localhost:8080/books")
      .then((response) => response.json())
      .then((data) => setBooks(data))
      .catch((error) => console.error("Error fetching books:", error));
  }, []);

  return (
    <div className="container">
      <h1 className="title">Books</h1>
      <div className="book-list">
        {books.map((book, index) => (
          <div key={index} className="book-item">
            <h2 className="book-name">{book.name}</h2>
            <p className="book-details">Author: {book.author}</p>
            <p className="book-details">Country: {book.country}</p>
            <p className="book-details">Year: {book.year}</p>
            <p className="book-details">Genre: {book.genre.join(", ")}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default App;

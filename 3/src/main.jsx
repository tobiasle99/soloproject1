import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'

// Import the functions you need from the SDKs you need

import { initializeApp } from "firebase/app";

import { getAnalytics } from "firebase/analytics";

// TODO: Add SDKs for Firebase products that you want to use

// https://firebase.google.com/docs/web/setup#available-libraries


// Your web app's Firebase configuration

// For Firebase JS SDK v7.20.0 and later, measurementId is optional

const firebaseConfig = {

  apiKey: "AIzaSyDN1zFatyc9f0WtpKMd8RxnemU3d0aNg8Q",

  authDomain: "bookstore-de600.firebaseapp.com",

  projectId: "bookstore-de600",

  storageBucket: "bookstore-de600.appspot.com",

  messagingSenderId: "528825964385",

  appId: "1:528825964385:web:d327bd23a1ef5efd0f5720",

  measurementId: "G-J2J55QP7GT"

};


// Initialize Firebase

const app = initializeApp(firebaseConfig);

const analytics = getAnalytics(app);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)

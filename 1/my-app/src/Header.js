import React from "react"
import ReactDOM from "react-dom/client"

export default function Header() {

    return(
      <header>
        <nav className="navbar">
          <img src="logo192.png" className="logo" />
          <ul className="nav-items">
            <li>Pricing</li>
            <li>About</li>
            <li>Contact</li>
          </ul>
        </nav>
      </header>
    )
  }

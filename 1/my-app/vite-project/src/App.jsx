import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import MainContent from '../components/MainContent'
import Navbar from '../components/Navbar'

function App() {

  return (
  <div className='container'>
    <Navbar />
    <MainContent />
    
  </div>
  )
}

export default App

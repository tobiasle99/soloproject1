import React from "react";
import logo from "../../public/logo192.png"
export default function Navbar() {
    return (
        <nav className="nav">
            <img src={logo} className="logo" />
            <h3 className="facts">ReactFacts</h3>
            <h4 className="title">React Course - Project 1</h4>
        </nav>
    )
}
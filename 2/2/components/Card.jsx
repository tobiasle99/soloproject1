import React from "react";
import photo from "../images/katie-zeferes.png"
import star from "../images/star.png"

export default function Card() {
    return (
    <div className="card">
        <img className="photo" src={photo} alt="alt" />
        <div className="ratings">
            <img className="star" src={star} alt="star" />
            <div className="ratingvalue">5.0</div>
            <div className="ratingamount">(6)</div>
            <div className="origin">USA</div>
        </div>
        <div className="title">Life lessons with Katie Zaferes</div>
        <div className="pricing">From $136 </div>
    </div>
    )
}